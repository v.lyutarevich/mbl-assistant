package com.mbl.assistant.service.telegram;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UtilsTest {

    @Test
    void testTelegramEscapingWorksWell() {

        String result = Utils.escapeForTelegramMarkdownV2("sk_if asdasff ' qwd 'qwd qwd1,2d 1!! fdafsd \\ dasdfokij1! fdf ... !!");
        assertEquals(
                "sk\\_if asdasff ' qwd 'qwd qwd1,2d 1\\!\\! fdafsd \\ dasdfokij1\\! fdf \\.\\.\\. \\!\\!",
                result
        );

    }

}