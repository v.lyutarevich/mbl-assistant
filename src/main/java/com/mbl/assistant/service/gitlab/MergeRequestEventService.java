package com.mbl.assistant.service.gitlab;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mbl.assistant.config.TelegramBotProperties;
import com.mbl.assistant.gitlab.Employee;
import com.mbl.assistant.service.telegram.AssistantAbilityBot;
import com.mbl.assistant.service.telegram.Utils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.gitlab4j.api.Constants;
import org.gitlab4j.api.models.Reviewer;
import org.gitlab4j.api.utils.JacksonJson;
import org.gitlab4j.api.webhook.MergeRequestEvent;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.mbl.assistant.service.telegram.Utils.escapeForTelegramMarkdownV2;

@Slf4j
@Service
@RequiredArgsConstructor
public class MergeRequestEventService {
    private final ObjectMapper gitlabObjectMapper = new JacksonJson().getObjectMapper();
    @NonNull
    private final GitlabMrService mrService;
    @NonNull
    private final AssistantAbilityBot bot;
    @NonNull
    private final TelegramBotProperties telegramBotProperties;

    private static final String MERGE_REQUEST_HAS_BEEN_ASSIGNED =
            """
                Merge request "%s"\\([link to MR](%s)\\) assigned to %s\\.
                *Tittle:* %s
                *Description:* %s
                                
                You have 5 days to review it and leave a feedback\\.
                """;

    @SneakyThrows
    public void processRawEventPayload (String rawEventPayload) {
        var webhookEvent = gitlabObjectMapper.readValue(rawEventPayload, MergeRequestEvent.class);
        handleMergeRequestEvent(webhookEvent);
    }

    public void handleMergeRequestEvent(MergeRequestEvent event) {
        Optional.ofNullable(event)
                .map(MergeRequestEvent::getObjectAttributes)
                .map(MergeRequestEvent.ObjectAttributes::getState)
                .filter(Predicate.isEqual(Constants.MergeRequestState.OPENED.toValue()))
                .ifPresent(action -> handleNewOpenedMergeRequestEvent(event));
    }

    @SneakyThrows
    private void handleNewOpenedMergeRequestEvent(MergeRequestEvent event) {
        var mr = mrService.assignReviewerToMergeRequest(event.getObjectAttributes());
        var employees = mr.getReviewers().stream()
                .map(Reviewer::getId)
                .map(Employee::byGitlabUserId)
                .toList();
        var reviewerTelegramNicknames = employees
                .stream()
                .map(Employee::getTelegramUsername)
                .map(Utils::escapeForTelegramMarkdownV2)
                .map(username -> "@" + username)
                .collect(Collectors.joining(" "));

        var message = mergeRequestAssignedMessage(mr.getWebUrl(), mr.getTitle(), mr.getDescription(), reviewerTelegramNicknames);
        sendMessage(telegramBotProperties.commonNotificationsChatId(), message, true);
        employees.forEach(employee -> sendMessage(employee.getTelegramChatId(), message));
    }

    private void sendMessage(long chatId, String text) {
        sendMessage(chatId, text, false);
    }
    @SneakyThrows
    private void sendMessage(long chatId, String text, boolean silent) {
        var messageBuilder = SendMessage.builder()
                .chatId(chatId)
                .text(text)
                .parseMode(ParseMode.MARKDOWNV2);
        if (silent) {
            messageBuilder.disableNotification(true);
        }
        bot.silent().execute(messageBuilder.build());
    }

    private String mergeRequestAssignedMessage(String url, String mrTittle, String mrDesc, String reviewerTelegramNicknames) {
        return MERGE_REQUEST_HAS_BEEN_ASSIGNED.formatted(
                escapeForTelegramMarkdownV2(mrTittle), escapeForTelegramMarkdownV2(url),
                escapeForTelegramMarkdownV2(reviewerTelegramNicknames), escapeForTelegramMarkdownV2(mrTittle),
                escapeForTelegramMarkdownV2(mrDesc)
        );
    }

}
