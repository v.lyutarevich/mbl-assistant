package com.mbl.assistant.service.gitlab;

import com.mbl.assistant.config.GitlabProperties;
import com.mbl.assistant.gitlab.Employee;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.MergeRequestApi;
import org.gitlab4j.api.models.*;
import org.gitlab4j.api.webhook.EventMergeRequest;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.mbl.assistant.service.telegram.Utils.getRandomElements;

@Service
@RequiredArgsConstructor
public class GitlabMrService {
    @NonNull
    private GitLabApi gitLabApi;
    @NonNull
    private GitlabProperties gitLabProps;

    private MergeRequestApi getMrApi() {
        return gitLabApi.getMergeRequestApi();
    }

    @SneakyThrows
    public List<MergeRequest> getMergeRequests(Employee employee) {
        var mergeRequestFilter = getDefaultMergeRequestFilter().withAuthorId(employee.getGitlabUserId());
        return getMrApi().getMergeRequests(mergeRequestFilter);
    }

    @SneakyThrows
    public List<MergeRequest> getMergeRequestsToReview(Employee employee) {
        List<MergeRequest> mergeRequests = getMrApi().getMergeRequests(getDefaultMergeRequestFilter());
        return mergeRequests.stream()
                .filter(mr -> mr.getReviewers().stream().anyMatch(r -> r.getId() == employee.getGitlabUserId()))
                .toList();
    }

    @SneakyThrows
    public MergeRequest assignReviewerToMergeRequest(EventMergeRequest eventMergeRequest) {
        List<ApprovalRule> approvalRuleFromMR = getMrApi().getApprovalRules(gitLabProps.projectId(), eventMergeRequest.getIid());

        var approvalRuleMBL = approvalRuleFromMR.stream()
                .filter(rule -> gitLabProps.approvalRuleName().equals(rule.getName()))
                .findAny()
                .orElseThrow();

        List<User> randomElements = getRandomElements(approvalRuleMBL.getEligibleApprovers(), approvalRuleMBL.getApprovalsRequired());
        var projectId = eventMergeRequest.getTargetProjectId();
        var mergeRequestId = eventMergeRequest.getIid();
        List<Long> reviewerIds = randomElements.stream()
                .map(User::getId)
                .toList();

        var params = new MergeRequestParams();
        params.withReviewerIds(reviewerIds);
        return getMrApi().updateMergeRequest(projectId, mergeRequestId, params);
    }

    @SneakyThrows
    public MergeRequest assignReviewerToMergeRequest(Employee employee, long projectId, long mergeRequestId) {
        return assignReviewerToMergeRequest(employee.getGitlabUserId(), projectId, mergeRequestId);
    }

    @SneakyThrows
    public MergeRequest assignReviewerToMergeRequest(long gitlabUserId, long projectId, long mergeRequestId) {
        var params = new MergeRequestParams();
        params.withReviewerIds(List.of(gitlabUserId));
        return getMrApi().updateMergeRequest(projectId, mergeRequestId, params);
    }

    private MergeRequestFilter getDefaultMergeRequestFilter() {
        MergeRequestFilter mergeRequestFilter = new MergeRequestFilter();
        return mergeRequestFilter.withGroupId(gitLabProps.groupId())
                .withState(Constants.MergeRequestState.OPENED)
                .withScope(Constants.MergeRequestScope.ALL);
    }
}
