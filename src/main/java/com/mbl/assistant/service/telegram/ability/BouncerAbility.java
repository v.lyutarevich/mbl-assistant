package com.mbl.assistant.service.telegram.ability;

import com.mbl.assistant.service.telegram.AssistantAbilityBot;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.abilitybots.api.bot.BaseAbilityBot;
import org.telegram.abilitybots.api.objects.Reply;
import org.telegram.abilitybots.api.util.AbilityExtension;
import org.telegram.abilitybots.api.util.AbilityUtils;
import org.telegram.telegrambots.meta.api.objects.Update;

import javax.annotation.PostConstruct;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

@Component
@RequiredArgsConstructor
public class BouncerAbility implements AbilityExtension {

    @NonNull
    private AssistantAbilityBot assistantBot;

    @PostConstruct
    public void activateAbility() {
        assistantBot.addExtension(this);
    }


    public Reply rejectRequestsFromNonWhitelistUsers() {
        BiConsumer<BaseAbilityBot, Update> action = (bot, upd) -> bot.silent().send(
                "The bot is under construction... Try again later.",
                AbilityUtils.getChatId(upd)
        );

        return Reply.of(action,
                Predicate.not(isMe()),
                Predicate.not(isMyWorkingAccount()),
                Predicate.not(isIuriiWorkingAccount())
        );
    }



    private Predicate<Update> isMe() {
        return update -> update.getMessage().getFrom().getId() == 237135484L;
    }

    private Predicate<Update> isMyWorkingAccount() {
        return update -> update.getMessage().getFrom().getId() == 2026924825L;
    }

    private Predicate<Update> isIuriiWorkingAccount() {
        return update -> update.getMessage().getFrom().getId() == 825535427L;
    }
}
