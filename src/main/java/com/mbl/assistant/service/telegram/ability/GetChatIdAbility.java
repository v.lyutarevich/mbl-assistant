package com.mbl.assistant.service.telegram.ability;


import com.mbl.assistant.service.telegram.AssistantAbilityBot;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import org.telegram.abilitybots.api.objects.Ability;
import org.telegram.abilitybots.api.objects.MessageContext;
import org.telegram.abilitybots.api.util.AbilityExtension;

import javax.annotation.PostConstruct;

import static org.telegram.abilitybots.api.objects.Locality.ALL;
import static org.telegram.abilitybots.api.objects.Privacy.PUBLIC;

@Component
@RequiredArgsConstructor
public class GetChatIdAbility implements AbilityExtension {
    private final AssistantAbilityBot assistantBot;

    @PostConstruct
    public void activateAbility() {
        assistantBot.addExtension(this);
    }


    public Ability getChatId() {
        return Ability.builder()
                .name("chatid")
                .privacy(PUBLIC)
                .locality(ALL)
                .setStatsEnabled(true)
                .info("Get current chat ID")
                .action(this::replyChatId)
                .build();
    }

    @SneakyThrows
    private void replyChatId(MessageContext ctx) {
        ctx.bot().silent().sendMd("`%s`".formatted(ctx.chatId()), ctx.chatId());
    }
}