package com.mbl.assistant.service.telegram;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class Utils {

    private static final Random rnd = new Random();
    private static final String escapingCharacter = "\\";
    private static final Set<String> charactersToEscape =
            Set.of("_", "*", "[", "]", "(", ")", "~", "`", ">", "#", "+", "-", "=", "|", "{", "}", ".", "!");


    public static String escapeForTelegramMarkdownV2(String text) {
        return charactersToEscape.stream()
                .reduce(text, (partialString, element) -> partialString.replace(element, escapingCharacter + element));
    }


    public static  <T> List<T> getRandomElements(List<T> elements, int amount) {
        if (elements == null || elements.isEmpty())
            return List.of();

        var toIterate = new ArrayList<>(elements);

        for (int i = 0; i < amount; i++) {
            int index = rnd.nextInt(toIterate.size() - i);
            toIterate.remove(index);
        }

        return toIterate;
    }

}
