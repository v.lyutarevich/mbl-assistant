package com.mbl.assistant.service.telegram.ability;


import com.mbl.assistant.service.gitlab.GitlabMrService;
import com.mbl.assistant.gitlab.Employee;
import com.mbl.assistant.service.telegram.AssistantAbilityBot;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.gitlab4j.api.models.MergeRequest;
import org.springframework.stereotype.Component;
import org.telegram.abilitybots.api.objects.Ability;
import org.telegram.abilitybots.api.util.AbilityExtension;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

import static org.telegram.abilitybots.api.objects.Locality.ALL;
import static org.telegram.abilitybots.api.objects.Privacy.PUBLIC;

@Component
@RequiredArgsConstructor
public class GitlabAbility implements AbilityExtension {

    @NonNull
    private GitlabMrService mrService;
    @NonNull
    private AssistantAbilityBot assistantBot;

    @PostConstruct
    public void activateAbility() {
        assistantBot.addExtension(this);
    }


    public Ability getMyMr() {
        return Ability.builder()
                .name("my_mr")
                .privacy(PUBLIC)
                .locality(ALL)
                .setStatsEnabled(true)
                .info("Returns merge requests where you are mentioned as a reviewer")
                .action(ctx -> {
                    Employee employee = Employee.byTelegramChatId(ctx.update().getMessage().getFrom().getId());
                    List<String> mergeRequests = mrService.getMergeRequests(employee)
                            .stream()
                            .map(MergeRequest::getWebUrl)
                            .toList();
                    String response = "Your merge requests:\n" + mergeRequests.stream().collect(Collectors.joining("\n"));
                    ctx.bot().silent().send(response, ctx.chatId());
                })
                .build();
    }

    public Ability haveToReview() {
        return Ability.builder()
                .name("to_review")
                .privacy(PUBLIC)
                .locality(ALL)
                .setStatsEnabled(true)
                .info("Returns merge requests where you are mentioned as a reviewer")
                .action(ctx -> {
                    Employee employee = Employee.byTelegramChatId(ctx.update().getMessage().getFrom().getId());
                    List<String> mergeRequests = mrService.getMergeRequestsToReview(employee)
                            .stream()
                            .map(MergeRequest::getWebUrl)
                            .toList();
                    String response = "You have to review these:\n" + mergeRequests.stream().collect(Collectors.joining("\n"));
                    ctx.bot().silent().send(response, ctx.chatId());
                })
                .build();
    }
}