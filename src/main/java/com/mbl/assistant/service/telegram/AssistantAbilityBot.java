package com.mbl.assistant.service.telegram;

import lombok.Getter;
import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.abilitybots.api.toggle.CustomToggle;
import org.telegram.abilitybots.api.util.AbilityExtension;
import org.telegram.telegrambots.bots.DefaultBotOptions;

public class AssistantAbilityBot extends AbilityBot {
    private static final CustomToggle toggle = new CustomToggle()
            .turnOff("ban")
            .toggle("promote", "upgrade");

    @Getter
    private final long creatorId;

    public AssistantAbilityBot(String botToken, String botNickname, long creatorId, DefaultBotOptions defaultBotOptions) {
        super(botToken, botNickname, defaultBotOptions);
        this.creatorId = creatorId;
        clearChatGRPConversations();
    }

    @Override
    public long creatorId() {
        return creatorId;
    }

    @Override
    public void addExtension(AbilityExtension extension) {
        super.addExtension(extension);
    }
    private void clearChatGRPConversations() {
        db.<Long>getSet("CONVERSATIONS").clear();
    }

}
