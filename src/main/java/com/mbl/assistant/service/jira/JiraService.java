package com.mbl.assistant.service.jira;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import net.rcarz.jiraclient.Issue;
import net.rcarz.jiraclient.JiraClient;
import net.rcarz.jiraclient.Project;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@RequiredArgsConstructor
public class JiraService {

    @NonNull
    private JiraClient jiraClient;

    @SneakyThrows
    @PostConstruct
    public void asd() {
        Issue issue = jiraClient.getIssue("MBL-8480");
        Project mbl = jiraClient.getProject("MBL");
    }

}
