package com.mbl.assistant.gitlab;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Random;
import java.util.stream.Stream;

@Getter
@AllArgsConstructor
public enum Employee {
    LYUTAREVICH(85L, 2026924825L, "lyutarevich"),
    KARPOV(239L, 825535427L, "iuriikarpov"),
    RUDOPYSOV(187L, 349306134L, "rudopysov"),
    PLESKA(240L, 409963592L, "sk_if"),
    SAMOYLOV(68L, 180329211L, "evidenzas"),
    GORLOV(152L, 420909475L, "Tisem"),
    BOYCHUK(51L, 121681998L, "ilboychuk");

    private static final Random rnd = new Random();

    private final long gitlabUserId;
    private final long telegramChatId;
    private final String telegramUsername;


    public static Employee byTelegramChatId(long telegramChatId) {
        return Stream.of(values())
                .filter(employee -> employee.telegramChatId == telegramChatId)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    public static Employee byGitlabUserId(long gitlabUserId) {
        return Stream.of(values())
                .filter(employee -> employee.gitlabUserId == gitlabUserId)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    public static Employee getRandomEmployee() {
        var values = values();
        return values[rnd.nextInt(values.length)];
    }
}
