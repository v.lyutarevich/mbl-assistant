package com.mbl.assistant.config;

import com.mbl.assistant.service.telegram.AssistantAbilityBot;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.generics.BotSession;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@Configuration
public class TelegramAbilityBotConfig {
    @Bean
    public AssistantAbilityBot getAssistantBot(TelegramBotProperties props) {
        return new AssistantAbilityBot(props.token(), props.nickname(), props.creatorId(), defaultBotOptions());
    }

    @Bean
    @SneakyThrows
    public BotSession initTelegramBots(AssistantAbilityBot assistantAbilityBot) {
        TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
        return botsApi.registerBot(assistantAbilityBot);
    }

    private DefaultBotOptions defaultBotOptions() {
        DefaultBotOptions defaultBotOptions = new DefaultBotOptions();
        defaultBotOptions.setMaxThreads(32);
        return defaultBotOptions;
    }

}
