package com.mbl.assistant.config;

import org.gitlab4j.api.GitLabApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GitlabApiConfig {

    @Bean
    public GitLabApi getGitlabApi(GitlabProperties properties) {
        return new GitLabApi(properties.baseUrl(), properties.accessToken());
    }

}
