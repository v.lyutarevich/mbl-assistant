package com.mbl.assistant.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("telegram.bot")
public record TelegramBotProperties(
        String token,
        String nickname,
        Long creatorId,
        Long commonNotificationsChatId
) {
}
