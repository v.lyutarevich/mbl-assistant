package com.mbl.assistant.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.HashMap;
import java.util.Map;

@Configuration
@RequiredArgsConstructor
public class WebClientConfiguration {
    private final GitlabProperties gitlabProperties;

    @Bean
    public WebClient gitlabWebClient() {
        Map<String, String> defaultQueryParams = new HashMap<>();
        defaultQueryParams.put("access_token", gitlabProperties.accessToken());

        return WebClient.builder()
                .baseUrl(gitlabProperties.baseUrl())
                .defaultUriVariables(defaultQueryParams)
                .build();
    }
}
