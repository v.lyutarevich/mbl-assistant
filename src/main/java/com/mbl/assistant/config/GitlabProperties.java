package com.mbl.assistant.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("gitlab")
public record GitlabProperties(
    String baseUrl,
    String accessToken,
    long groupId,
    long projectId,
    String approvalRuleName
) {
}
