package com.mbl.assistant.config;

import lombok.SneakyThrows;
import net.rcarz.jiraclient.BasicCredentials;
import net.rcarz.jiraclient.ICredentials;
import net.rcarz.jiraclient.JiraClient;
import net.rcarz.jiraclient.RestClient;
import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.Map;

import static com.mbl.assistant.ReflectionUtils.setInstanceValue;

@Configuration
public class JiraConfiguration {

    @Bean
    @SneakyThrows
    public JiraClient getJiraClient(@Value("${jira.base-url}") String baseUrl,
                                    @Value("${jira.username}") String username,
                                    @Value("${jira.password}") String password) {
        var credentials = new BasicCredentials(username, password);
        var jiraClient = new JiraClient(baseUrl, credentials);

        setInstanceValue(jiraClient, "restclient", overrideRestClient(credentials, baseUrl));

        return jiraClient;
    }


    private RestClient overrideRestClient(BasicCredentials credentials, String baseUrl) {
        return new MyRestClient(new DefaultHttpClient(), credentials, URI.create(baseUrl));
    }



    //Overriding this shit coz otherwise we gonna get NULL inside our link (because of author's implementation of method "buildURI")
    private static class MyRestClient extends RestClient {
        URI oneMoreUri;

        public MyRestClient(HttpClient httpclient, URI uri) {
            super(httpclient, uri);
            this.oneMoreUri = uri;
        }

        public MyRestClient(HttpClient httpclient, ICredentials creds, URI uri) {
            super(httpclient, creds, uri);
            this.oneMoreUri = uri;
        }

        @Override
        public URI buildURI(String path) throws URISyntaxException {
            return super.buildURI(path);
        }

        @Override
        public URI buildURI(String path, Map<String, String> params) throws URISyntaxException {
            URIBuilder ub = new URIBuilder(this.oneMoreUri);
            if (ub.getPath() != null)
                ub.setPath(ub.getPath() + path);
            else
                ub.setPath(path);
            if (params != null) {
                Iterator i$ = params.entrySet().iterator();

                while(i$.hasNext()) {
                    Map.Entry<String, String> ent = (Map.Entry)i$.next();
                    ub.addParameter(ent.getKey(), ent.getValue());
                }
            }

            return ub.build();
        }
    }
}
