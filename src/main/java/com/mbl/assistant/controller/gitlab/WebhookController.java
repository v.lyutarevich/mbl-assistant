package com.mbl.assistant.controller.gitlab;

import com.mbl.assistant.service.gitlab.MergeRequestEventService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.gitlab4j.api.webhook.MergeRequestEvent;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("gitlab/webhook")
public class WebhookController {
    @NonNull
    private final MergeRequestEventService mergeRequestEventService;

    @PostMapping(value = "mr", consumes = {"application/json"})
    public Mono<Void> onMergeRequest(@RequestBody MergeRequestEvent webhookEventPayload) {
        return Mono.fromRunnable(() -> mergeRequestEventService.handleMergeRequestEvent(webhookEventPayload));
    }
}
