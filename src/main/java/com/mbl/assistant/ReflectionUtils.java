package com.mbl.assistant;

import java.lang.reflect.Field;

public class ReflectionUtils {

    private ReflectionUtils() {}

    /**
     * Returns an object containing the value of any field of an object instance (even private).
     * @param classInstance An Object instance.
     * @param fieldName The name of a field in the class instantiated by classInstance
     * @return An Object containing the field value.
     * @throws SecurityException .
     * @throws NoSuchFieldException .
     * @throws IllegalArgumentException .
     * @throws IllegalAccessException .
     */
    public static Object getInstanceValue(final Object classInstance, final String fieldName) throws SecurityException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException {

        // Get the private field
        final Field field = classInstance.getClass().getDeclaredField(fieldName);
        // Allow modification on the field
        field.setAccessible(true);
        // Return the Obect corresponding to the field
        return field.get(classInstance);

    }

    public static void setInstanceValue(final Object classInstance, final String fieldName, final Object fieldValue) throws SecurityException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException {

        // Get the private field
        final Field field = classInstance.getClass().getDeclaredField(fieldName);
        // Allow modification on the field
        field.setAccessible(true);
        // Change the Field corresponding to the Object
        field.set(classInstance, fieldValue);
    }
}
