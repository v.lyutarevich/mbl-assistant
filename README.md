# Repository Readme

This repository contains a Spring Boot application that integrates with GitLab and Telegram. The application is configured using the following environment variables:

## Configuration

### Server

- `APP_PORT`: The port on which the application will run. Default is `9090`.

### GitLab

- `GITLAB_BASE_URL`: The base URL of your GitLab instance. Default is `https://gitlab.com`.
- `GITLAB_ACCESS_TOKEN`: The access token for your GitLab account. Replace `XXX` with your actual token.

### Telegram

- `TELEGRAM_BOT_TOKEN`: The token for your Telegram bot.
- `TELEGRAM_BOT_NICKNAME`: The nickname for your Telegram bot.
- `TELEGRAM_BOT_CREATOR_ID`: The creator ID for your Telegram bot.

## Getting Started

1. Clone the repository.
2. Set the required environment variables.
3. Run the application using `./mvnw spring-boot:run` or by running the main class in your IDE.
4. The application will start on the specified port and will be ready to receive webhook events from GitLab and interact with your Telegram bot.

## Contributing

Feel free to submit issues or pull requests if you have any suggestions or improvements for the project.